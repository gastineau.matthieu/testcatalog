# LICENCES

Le code source de ce dépôt est publié sous [licence MIT](#licence-mit).

Sauf mention de propriété intellectuelle détenue par des tiers (notamment
un crédit sur certaines images), les contenus de ce dépôt sont publiés sous [licence Ouverte 2.0](#licence-ouverte-20open-licence-20).
